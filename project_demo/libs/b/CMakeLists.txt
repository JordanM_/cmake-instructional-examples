cmake_minimum_required(VERSION 3.9.6 FATAL_ERROR)

project(b VERSION 0.1 LANGUAGES CXX)
add_library(lib_b b.cpp)
target_include_directories(lib_b PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
target_link_libraries(lib_b PRIVATE lib_c)