/* Required include files for C STDIO and Nucleus PLUS kernel services */
#include <stdio.h>
#include "nucleus.h"
#include "kernel/nu_kernel.h"

/* Define the main task's stack size */
#define HELLO_WORLD_TASK_STACK_SIZE      (NU_MIN_STACK_SIZE * 2048)

/* Define the main task's priority */
#define HELLO_WORLD_TASK_PRIORITY   26

/* Define the main task's time slice */
#define HELLO_WORLD_TASK_TIMESLICE  20

/* Statically allocate the main task's control block */
static NU_TASK Task_Control_Block;

/* Prototype for the main task's entry function */
static VOID Main_Task_Entry(UNSIGNED argc, VOID *argv);

/***********************************************************************
 * *
 * *   FUNCTION
 * *
 * *       Application_Initialize
 * *
 * *   DESCRIPTION
 * *
 * *       Demo application entry point - initializes Nucleus Plus
 * *       demonstration application by creating and initializing necessary
 * *       tasks, memory pools, and communication components.
 * *
 * ***********************************************************************/
VOID Application_Initialize(NU_MEMORY_POOL* mem_pool,
                            NU_MEMORY_POOL* uncached_mem_pool)
{
    VOID *pointer;
    STATUS status;

    /* Reference unused parameters to avoid toolset warnings */
    NU_UNUSED_PARAM(uncached_mem_pool);

    /* Allocate memory for the main task */
    status = NU_Allocate_Memory(mem_pool, &pointer, HELLO_WORLD_TASK_STACK_SIZE, NU_NO_SUSPEND);

    /* Check to see if previous operation was successful */
    if (status == NU_SUCCESS)
    {
        /* Create task 0.  */
        status = NU_Create_Task(&Task_Control_Block, "MAIN", Main_Task_Entry, \
                                0, NU_NULL, pointer, HELLO_WORLD_TASK_STACK_SIZE, \
                                HELLO_WORLD_TASK_PRIORITY, HELLO_WORLD_TASK_TIMESLICE, \
                                NU_PREEMPT, NU_START);
    }

    /* Check to see if previous operations were successful */
    if (status != NU_SUCCESS)
    {
        /* Loop forever */
        while(1);
    }
}

/***********************************************************************
 * *
 * *   FUNCTION
 * *
 * *       Main_Task_Entry
 * *
 * *   DESCRIPTION
 * *
 * *       Entry function for the main task. This task prints a hello world
 * *       message.
 * *
 * ***********************************************************************/
static VOID Main_Task_Entry(UNSIGNED argc, VOID *argv)
{
    /* Reference all parameters to ensure no toolset warnings */
    NU_UNUSED_PARAM(argc);
    NU_UNUSED_PARAM(argv);

    printf("Hello, World!\n");
    return;
}